const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const basePath = __dirname;
const distPath = 'dist';

module.exports = {
  // mode:'development',
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },  
  entry: ['@babel/polyfill','./src/main.ts'],
  output:{
    path: path.resolve(basePath, distPath),
    filename:'bundle.js'
  },
  module: {
    rules:[
      {
        test:/\.js/,
        exclude:'/node_modules/',
        use:['babel-loader']  
      },{
        test:/\.ts/,
        exclude: '/node_modules/',
        use: ['ts-loader']
      },{
        test:/\.scss$/,
        use:[
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      },{
        test:/\.pug$/,
        use:['pug-loader']
      }
    ]
  },  
  plugins:[
    new HtmlWebpackPlugin({
      filename:'index.html',
      template:'./src/templates/index.pug'     
    }),
    new MiniCssExtractPlugin({
      filename:'styles.css'
    })
  ],
  devServer:{
    contentBase: path.join(basePath, distPath),
    compress:true,
    port:9000
  }
}